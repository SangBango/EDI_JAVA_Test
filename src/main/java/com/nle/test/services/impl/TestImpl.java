package com.nle.test.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nle.test.controller.Utils;
import com.nle.test.dto.LogDto;
import com.nle.test.model.LogModel;
import com.nle.test.repositories.LogRepository;
import com.nle.test.services.Test;


@Service
public class TestImpl implements Test {

	@Autowired
	LogRepository logRepository;
	
	@Autowired
	Utils utils;
	
	@Override
	public LogDto getBookingId(String idrequestbooking) {
		// TODO Auto-generated method stub
		LogModel logModel = logRepository.findByIdrequestbooking(idrequestbooking);
		
		LogDto returnValue = new LogDto();
		
		BeanUtils.copyProperties(logModel, returnValue);
		return returnValue;
	}

	@Override
	public LogDto createLog(LogDto log) {
		// TODO Auto-generated method stub		
		ModelMapper modelMapper = new ModelMapper();
		LogModel data = modelMapper.map(log, LogModel.class);
		
//		save data
		String id = utils.generateId(2);
		data.setIdRequestBooking("BR_"+id);
		
		LogModel storeData = logRepository.save(data);
		LogDto returnValue = modelMapper.map(storeData, LogDto.class);
		
		return returnValue;
	}

	@Override
	public LogDto updateLog(String idrequestbooking, LogDto log) {
		// TODO Auto-generated method stub
		LogDto returnValue = new LogDto();
		
		LogModel bookingUpdate = logRepository.findByIdrequestbooking(idrequestbooking);
		
//		save data		
		bookingUpdate.setId_platform(log.getId_platform());
		bookingUpdate.setNama_platform(log.getNama_platform());
		bookingUpdate.setDoc_type(log.getDoc_type());
		bookingUpdate.setTerm_of_payment(log.getTerm_of_payment());
		
		LogModel updated = logRepository.save(bookingUpdate);
		BeanUtils.copyProperties(updated, returnValue);
		
		return returnValue;
	}

	@Override
	public List<LogDto> getLogs() {
		// TODO Auto-generated method stub
		List<LogDto> returnValue = new ArrayList<LogDto>();
		ModelMapper modelMapper = new ModelMapper();
		
		List<LogModel> bookings = logRepository.findAll();
		
		for (LogModel booking : bookings) {
			returnValue.add(modelMapper.map(booking, LogDto.class) );
		}
		
		return returnValue;
	}

	@Override
	public boolean deleteLog(String idrequestbooking) {
		// TODO Auto-generated method stub
		try {
			logRepository.deleteById(idrequestbooking);
			return true;
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		}
	}

}
