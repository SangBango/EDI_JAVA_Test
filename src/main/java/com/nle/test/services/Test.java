package com.nle.test.services;

import java.util.List;

import com.nle.test.dto.LogDto;

public interface Test {
	LogDto getBookingId(String idrequestbooking);
	LogDto createLog(LogDto log);
	LogDto updateLog(String idrequestbooking, LogDto log);
	List<LogDto> getLogs();
	boolean deleteLog(String idrequestbooking);
}
