package com.nle.test.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nle.test.model.LogModel;

@Repository
public interface LogRepository extends JpaRepository<LogModel, String> {
	LogModel findByIdrequestbooking(String idrequestbooking);
}
