package com.nle.test.controller;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nle.test.dto.LogDto;
import com.nle.test.model.request.LogRequest;
import com.nle.test.model.response.LogResponse;
import com.nle.test.services.Test;

@RestController
public class Controller {
	@Autowired
	Test testService;
	
	@GetMapping(
			path = "/booking_detail_by_id", 
			produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public LogResponse getLog(@RequestParam(value = "idrequestbooking") String idrequestbooking) {
		LogResponse returnValue = new LogResponse();
		
		LogDto logDto = testService.getBookingId(idrequestbooking);
	
		if (logDto != null) {
			java.lang.reflect.Type listType = new TypeToken<LogResponse>() {
			}.getType();
			returnValue = new ModelMapper().map(logDto, listType);
		}
		
		return returnValue;
	}
	
	@GetMapping(
			path = "/all_booking_request", 
			produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public List<LogResponse> getLogs() {
		List<LogResponse> returnValue = new ArrayList<>();
		
		List<LogDto> logs = testService.getLogs();
		
		if (logs != null && !logs.isEmpty()) {
			java.lang.reflect.Type listType = new TypeToken<List<LogResponse>>() {
			}.getType();
			returnValue = new ModelMapper().map(logs, listType);
		}
		
		return returnValue;
	}
	
	@PostMapping(
			path = "/add_booking_request",
			consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
			produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}
			)
	public LogResponse addBookingRequest(@RequestBody LogRequest log) {
		LogResponse returnValue = new LogResponse();
		
		ModelMapper modelMapper = new ModelMapper();
		LogDto logDto = modelMapper.map(log, LogDto.class);
		
		LogDto bookingRequested = testService.createLog(logDto);
		returnValue = modelMapper.map(bookingRequested, LogResponse.class);
		
		return returnValue;
	}
	
	@DeleteMapping(path = "/delete_booking_request")
    public ResponseEntity<String> deletePost(@RequestParam(value = "idrequestbooking") String idrequestbooking) {

        var isRemoved = testService.deleteLog(idrequestbooking);

        if (!isRemoved) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(idrequestbooking, HttpStatus.OK);
    }
	
	@PutMapping(path = "/update_booking_data",
	consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
	produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public LogResponse updateRequest(@RequestParam(value = "idrequestbooking") String idrequestbooking,
			@RequestBody LogRequest log) {
		LogResponse returnValue = new LogResponse();
		
		LogDto logDto = new LogDto();
		BeanUtils.copyProperties(log, logDto);
		
		LogDto updatedAgenda = testService.updateLog(idrequestbooking, logDto);
		BeanUtils.copyProperties(updatedAgenda, returnValue);
		
		return returnValue;
	}
}
