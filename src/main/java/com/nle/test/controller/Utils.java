package com.nle.test.controller;

import java.security.SecureRandom;
import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class Utils {
	private final Random RANDOM = new SecureRandom();
	private final String NUMBER = "0123456789";
	
	private String generateRandomString(int length) {
		StringBuilder returnValue = new StringBuilder(length);
		
		for (int i = 0; i < length; i++) {
			returnValue.append(NUMBER.charAt(RANDOM.nextInt(NUMBER.length())));
		}
		
		return new String(returnValue);
	}
	
	public String generateId(int length) {
		// TODO Auto-generated method stub
		return generateRandomString(length);
	}
}
