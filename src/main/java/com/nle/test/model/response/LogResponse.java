package com.nle.test.model.response;

public class LogResponse {
	private String idrequestbooking;
	private String id_platform;
	private String nama_platform;
	private String doc_type;
	private String term_of_payment;
	public String getIdRequestBooking() {
		return idrequestbooking;
	}
	public void setIdRequestBooking(String idrequestbooking) {
		this.idrequestbooking = idrequestbooking;
	}
	public String getId_platform() {
		return id_platform;
	}
	public void setId_platform(String id_platform) {
		this.id_platform = id_platform;
	}
	public String getNama_platform() {
		return nama_platform;
	}
	public void setNama_platform(String nama_platform) {
		this.nama_platform = nama_platform;
	}
	public String getDoc_type() {
		return doc_type;
	}
	public void setDoc_type(String doc_type) {
		this.doc_type = doc_type;
	}
	public String getTerm_of_payment() {
		return term_of_payment;
	}
	public void setTerm_of_payment(String term_of_payment) {
		this.term_of_payment = term_of_payment;
	}
}
