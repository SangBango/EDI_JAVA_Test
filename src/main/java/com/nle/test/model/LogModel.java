package com.nle.test.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="log_nle_api01")
public class LogModel {
	
	@Id
	@Column(nullable = false, length = 255, unique = true)
	private String idrequestbooking;
	@Column(nullable = false, length = 20)
	private String id_platform;
	@Column(nullable = false, length = 20)
	private String nama_platform;
	@Column(nullable = false, length = 20)
	private String doc_type;
	@Column(nullable = false, length = 5)
	private String term_of_payment;
	
	public String getIdRequestBooking() {
		return idrequestbooking;
	}
	public void setIdRequestBooking(String idrequestbooking) {
		this.idrequestbooking = idrequestbooking;
	}
	public String getId_platform() {
		return id_platform;
	}
	public void setId_platform(String id_platform) {
		this.id_platform = id_platform;
	}
	public String getNama_platform() {
		return nama_platform;
	}
	public void setNama_platform(String nama_platform) {
		this.nama_platform = nama_platform;
	}
	public String getDoc_type() {
		return doc_type;
	}
	public void setDoc_type(String doc_type) {
		this.doc_type = doc_type;
	}
	public String getTerm_of_payment() {
		return term_of_payment;
	}
	public void setTerm_of_payment(String term_of_payment) {
		this.term_of_payment = term_of_payment;
	}
}
